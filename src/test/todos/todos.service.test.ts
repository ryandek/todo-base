import { expect } from "chai";
import { TodosService } from "../../../server/todo-api/src/todos/todos.service";
import { Model } from "mongoose";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import {
  ITodo,
  ITodosService,
} from "../../../server/todo-api/src/todos/interfaces/index";
import { CreateTodoDto } from "../../../server/todo-api/src/todos/dto/createTodo.dto";
import { debug } from "console";

describe("Test class TodosService", () => {
  it("TodosService-findAll", async () => {
    // Arguments
    const todoModel1 = undefined;

    // Method call
    const todosService = new TodosService(todoModel1);
    const result = await todosService.findAll();

    // Expect result
    expect(result).to.be.not.undefined;
  });

  it("TodosService-findOne", async () => {
    // Arguments
    const todoModel2 = undefined;
    const options1 = undefined;

    // Method call
    const todosService = new TodosService(todoModel2);
    const result = await todosService.findOne(options1);

    // Expect result
    expect(result).to.be.not.undefined;
  });

  it("TodosService-findById", async () => {
    // Arguments
    const todoModel3 = undefined;
    const ID1 = "Oha";

    // Method call
    const todosService = new TodosService(todoModel3);
    const result = await todosService.findById(ID1);

    // Expect result
    expect(result).to.be.not.undefined;
  });

  it("TodosService-create", async () => {
    // Arguments
    const todoModel4 = undefined;
    const createTodoDto1 = undefined;

    // Method call
    const todosService = new TodosService(todoModel4);
    const result = await todosService.create(createTodoDto1);

    // Expect result
    expect(result).to.be.not.undefined;
  });

  it("TodosService-update", async () => {
    // Arguments
    const todoModel5 = undefined;
    const ID2 = "Oha";
    const newValue1 = undefined;

    // Method call
    const todosService = new TodosService(todoModel5);
    const result = await todosService.update(ID2, newValue1);

    // Expect result
    expect(result).to.be.not.undefined;
  });

  it("TodosService-delete", async () => {
    // Arguments
    const todoModel6 = undefined;
    const ID3 = "Oha";

    // Method call
    const todosService = new TodosService(todoModel6);
    const result = await todosService.delete(ID3);

    // Expect result
    expect(result).to.be.not.undefined;
  });
});
