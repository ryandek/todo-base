/* tslint:disable:no-unused-variable */
import {
    HttpClientTestingModule,
    HttpTestingController,
  } from '@angular/common/http/testing';
import { TestBed, async } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';

// Define global references for injections.

describe('AppComponent', () => {
  let comp : AppComponent
  let el: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        DataService
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'todo-client-app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    console.log(app.title);
    expect(app.title).toEqual('todo-client-app');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Todo App');
  }));

  it('Testing Div Classes to be used', function () {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    // main div classes to be used
    expect(compiled.querySelector('.data')).toBeTruthy();
    expect(compiled.querySelector('.list-unstyled')).toBeTruthy();
    expect(compiled.querySelector('.form-group')).toBeTruthy();
    expect(compiled.querySelector('.container')).toBeTruthy();
    expect(compiled.querySelector('.form-control')).toBeTruthy();
  });
});