/* tslint:disable:no-unused-variable */
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';

// setting global variables
const apiBaseUrl ="http://heroku-gitlab-env-prod-dep.herokuapp.com";
var testResponse;

describe('Service: DataService', () => {
  let service: DataService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DataService],
    });
    service = TestBed.get(DataService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return a todo', fakeAsync(() => {
    testResponse = {
      resultCount: 1,
      results: [
        {
          _id: '5eb95c9f2caf72001dd6f71e',
          text: 'test',
          complete: false,
        },
      ],
    };

    service.create('test');

    const testReq = httpTestingController.expectOne(
      apiBaseUrl+ '/todos'
    );
    expect(testReq.request.method).toEqual('POST');

    
    testReq.flush(testResponse);

    // Call tick which actually processes the response
    tick();

    // Run our tests
    expect(testResponse.resultCount).toBe(1);
    expect(testResponse.results[0]._id).toBe('5eb95c9f2caf72001dd6f71e');
    expect(testResponse.results[0].text).toBe('test');
    expect(testResponse.results[0].complete).toBe(false);
  }));
});
