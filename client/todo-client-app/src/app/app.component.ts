import { Component, OnInit,ElementRef, ViewChild  } from '@angular/core';
import { animate, keyframes, style, transition, trigger, } from '@angular/animations';
import { DataService } from './data.service';
import { Todo } from './Todo';
import { Observable,BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('moveInLeft', [
      transition('void=> *', [
        style({ transform: 'translateX(300px)' }),
        animate(
          200,
          keyframes([
            style({ transform: 'translateX(300px)' }),
            style({ transform: 'translateX(0)' }),
          ])
        ),
      ]),

      transition('*=>void', [
        style({ transform: 'translateX(0px)' }),
        animate(
          100,
          keyframes([
            style({ transform: 'translateX(0px)' }),
            style({ transform: 'translateX(300px)' }),
          ])
        ),
      ]),
    ]),
  ],
})
export class AppComponent implements OnInit {
  title = 'todo-client-app';
  todos: Observable<Todo[]>;
  singleTodo: Observable<Todo>;
  todoForm: FormGroup;
  count: number;
  countActive: number;
  countCompleted: number;
  changeDeleteText: boolean;
  @ViewChild('todo') todoText: ElementRef;
  submitted = false;
  
  constructor(
    private todoService: DataService,
    private formBuilder: FormBuilder) {
    this.changeDeleteText = false;
    this.todoForm = this.formBuilder.group({
      todo: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.todos = this.todoService.todos;
    this.todos.subscribe(result => {
      this.count = result.length;
      this.count = result.length;
      this.countCompleted = result.filter(item => item.complete).length;
      this.countActive = this.count - this.countCompleted;
    });
    this.singleTodo = this.todoService.todos.pipe(
      map(todos => todos.find(item => item._id === '1'))
    );

    this.todoService.loadAll();
  }

  onSubmit(value: string) {
    if(value !=""){
      this.todoText.nativeElement.value = '';
      this.submitted =true;
      this.todoService.create(value);
    }
  }

  deleteTodo(todoId: string) {
    this.todoService.remove(todoId);
  }

  updateTodo(todo: Todo) {
    this.todoService.update(todo);
  }

  findByStatus(todoStatus) {
    this.todoService.load(todoStatus);
  }

  markComplete(todo: Todo) {
    this.updateTodo({_id:todo._id, text: todo.text, complete: true});
  }

  enableEdit (id,text) {
   const component = this;
   $(".edit-container-"+id).html(`<div class="col-sm-9"><input type="text" value="${text}" class="form-control editText-${id} form-control-sm"></div><button type="button" idValue="${id}" class="col-sm-3 btn btn-success update-button">Update</button>`);
   $('.list-unstyled').on('click', '.update-button', function () {
    const idValue=$(this).attr("idValue");
    const editText = $(".editText-"+idValue).val();
    component.updateTodo({_id:idValue, text: editText, complete: false});
  }); 
  }
}
