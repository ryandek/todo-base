import { Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TodosModule } from './todos/todos.module';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGODB_URI || 'mongodb://heroku_brkcs128:trouts111@ds253398.mlab.com:53398/heroku_brkcs128'),
    TodosModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
