import { Document } from 'mongoose';

export interface ITodo extends Document {
    readonly _id: string;
    readonly text: string;
    readonly complete: boolean;
}