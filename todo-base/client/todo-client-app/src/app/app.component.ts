import { Component, OnInit } from '@angular/core';
import { animate, keyframes, style, transition, trigger, } from '@angular/animations';
import { DataService } from './data.service';
import { Todo } from './Todo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('moveInLeft', [
      transition('void=> *', [
        style({ transform: 'translateX(300px)' }),
        animate(
          200,
          keyframes([
            style({ transform: 'translateX(300px)' }),
            style({ transform: 'translateX(0)' }),
          ])
        ),
      ]),

      transition('*=>void', [
        style({ transform: 'translateX(0px)' }),
        animate(
          100,
          keyframes([
            style({ transform: 'translateX(0px)' }),
            style({ transform: 'translateX(300px)' }),
          ])
        ),
      ]),
    ]),
  ],
})
export class AppComponent implements OnInit {
  todos: Observable<Todo[]>;
  singleTodo: Observable<Todo>;
  todoForm: FormGroup;
  count: number;
  countActive: number;
  countCompleted: number;

  constructor(
    private todoService: DataService,
    private formBuilder: FormBuilder) {

    this.todoForm = this.formBuilder.group({
      todo: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.todos = this.todoService.todos;
    this.todos.subscribe(result => {
      this.count = result.length;
      this.countCompleted = result.filter(item => item.complete).length;
      this.countActive = this.count - this.countCompleted;
    });
    this.singleTodo = this.todoService.todos.pipe(
      map(todos => todos.find(item => item._id === '1'))
    );

    this.todoService.loadAll();
  }

  onSubmit(value: string) {
    this.todoService.create(value);
  }

  deleteTodo(todoId: string) {
    this.todoService.remove(todoId);
  }

  updateTodo(todo: Todo) {
    this.todoService.update(todo);
  }
}
