import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

export class TodoItem {
  constructor(
    public _id: string,
    public text: string,
    public complete: boolean
  ) { }
}

@Injectable({
  providedIn: 'root',
})
export class DataService {
  // tslint:disable-next-line:variable-name
  private _todos = new BehaviorSubject<Todo[]>([]);
  private baseUrl = 'http://heroku-gitlab-env-prod-dep.herokuapp.com';
  private dataStore: { todos: Todo[] } = { todos: [] };
  readonly todos = this._todos.asObservable();
  results: TodoItem[];

  constructor(private http: HttpClient) {
  }

  loadAll() {
    this.http.get<Todo[]>(`${this.baseUrl}/todos`).subscribe(data => {
      this.dataStore.todos = data;
      this._todos.next(Object.assign({}, this.dataStore).todos);
    }, error => console.log('Could not load todos.'));
  }

  load(id: string) {
    this.http.get<Todo>(`${this.baseUrl}/todos/${id}`).subscribe(data => {
      let notFound = true;

      this.dataStore.todos.forEach((item, index) => {
        if (item._id === data._id) {
          this.dataStore.todos[index] = data;
          notFound = false;
        }
      });

      if (notFound) {
        this.dataStore.todos.push(data);
      }

      this._todos.next(Object.assign({}, this.dataStore).todos);
    }, error => console.log('Could not load todo.'));
  }

  create(todo: string) {
    this.http.post<Todo>(`${this.baseUrl}/todos`, {
      _id: '0',
      text: todo,
      complete: false,
    }).subscribe(data => {
      this.dataStore.todos.push(data);
      this._todos.next(Object.assign({}, this.dataStore).todos);
      return new TodoItem(data._id, data.text, data.complete);
    }, error => console.log('Could not create todo.'));
  }

  update(todo: Todo) {
    this.http.put<Todo>(`${this.baseUrl}/todos/${todo._id}`, JSON.stringify(todo)).subscribe(data => {
      this.dataStore.todos.forEach((t, i) => {
        if (t._id === todo._id) {
          this.dataStore.todos[i] = data;
        }
      });

      this._todos.next(Object.assign({}, this.dataStore).todos);
    }, error => console.log('Could not update todo.'));
  }

  remove(todoId: number | string) {
    this.http.delete(`${this.baseUrl}/todos/${todoId}`).subscribe(response => {
      this.dataStore.todos.forEach((t, i) => {
        if (t._id === todoId) {
          this.dataStore.todos.splice(i, 1);
        }
      });

      this._todos.next(Object.assign({}, this.dataStore).todos);
    }, error => console.log('Could not delete todo.'));
  }
}


export interface Todo {
  _id: string;
  text: string;
  complete: boolean;
}

