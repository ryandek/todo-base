/* tslint:disable:no-unused-variable */
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';

describe('Service: DataService', () => {
  let service: DataService;
  let httpTestingController: HttpTestingController;
  let baseUrl: 'http://heroku-gitlab-env-prod-dep.herokuapp.com';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DataService],
    });
    service = TestBed.get(DataService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should not immediately connect to the server', () => {
    httpTestingController.expectNone({});
});

  it('should return a todo', fakeAsync(() => {
    let response = {
      resultCount: 1,
      results: [
        {
          _id: '5eb95c9f2caf72001dd6f71e',
          text: 'test',
          complete: false,
        },
      ],
    };

    service.create('test');

    const req = httpTestingController.expectNone(
      baseUrl + '/todos/5eb95c9f2caf72001dd6f71e'
    );
    const req = httpTestingController
    .expectOne(req => req.method === 'POST' && req.url === baseUrl);
    expect(req.request.method).toEqual('POST');
    req.flush(response);

    // Call tick which actually processes the response
    tick();

    // Run our tests
    expect(service.results.length).toBe(1);
    expect(service.results[0]._id).toBe('5eb95c9f2caf72001dd6f71e');
    expect(service.results[0].text).toBe('test');
    expect(service.results[0].complete).toBe(false);
  }));
});
