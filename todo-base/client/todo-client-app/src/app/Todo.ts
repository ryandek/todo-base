export interface Todo {
  readonly _id: string;
  readonly text: string;
  readonly complete: boolean;
}
