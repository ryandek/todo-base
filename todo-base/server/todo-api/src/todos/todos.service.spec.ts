import { Test, TestingModule } from '@nestjs/testing';
import { TodoService } from './todos.service';
import { TodosController } from './todos.controller';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ITodo, ITodosService } from './interfaces/index';
import { CreateTodoDto } from './dto/createTodo.dto';
import { debug } from 'console';

describe('TodoService', () => {
  let service: TodoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TodoService],
    }).compile();

    service = module.get<TodoService>(TodoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
