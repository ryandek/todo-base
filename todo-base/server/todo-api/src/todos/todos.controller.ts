import { Controller, Get, Response, HttpStatus, Param, Body, Post, Request, Patch, Delete } from '@nestjs/common';
import { TodoService } from './todos.service';
import { CreateTodoDto} from './dto/createTodo.dto';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('todos')
@Controller('todos')
export class TodosController {
    constructor(private readonly todosService: TodoService) {}

    @Get()
    public async getTodos(@Response() res) {
        const todos = await this.todosService.findAll();
        return res.status(HttpStatus.OK).json(todos);
    }

    @Get('find')
    public async findTodo(@Response() res, @Body() body) {
        const queryCondition = body;
        const todos = await this.todosService.findOne(queryCondition);
        return res.status(HttpStatus.OK).json(todos);
    }

    @Get(':id')
    public async getTodo(@Response() res, @Param('id') id: string){
        const todos = await this.todosService.findById(id);
        return res.status(HttpStatus.OK).json(todos);
    }

    @Post()
    @ApiResponse({ status: 201, description: 'The todo item has been successfully created.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async createTodo(@Response() res, @Body() createTodoDTO: CreateTodoDto) {

        const todo = await this.todosService.create(createTodoDTO);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Patch(':id')
    public async updateTodo(@Param('id') id: string, @Response() res, @Body() body) {

        const todo = await this.todosService.update(id, body);
        return res.status(HttpStatus.OK).json(todo);
    }

    @Delete(':id')
    public async deleteTodo(@Param('id') id: string, @Response() res) {

        const todo = await this.todosService.delete(id);
        return res.status(HttpStatus.OK).json(todo);
    }
}
